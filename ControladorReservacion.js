module.exports=function(app){
	return{
		add:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
					connection.release();
					res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
				}
				connection.query("INSERT INTO reservacion VALUES (NULL,'"+req.body.idUsuario+"','"+req.body.idEstancia+"');", function(err,row){
				if(err)
					throw err;
				else
					res.json({"mensaje":"Reservacion Agregado"});
				connection.release();
				});
			});
		},
				delete:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Delete from estancia where idReservacion="+req.body.idReservacion, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Reservacion eliminada"});
					connection.release();	
				});
			});	
		},
		list:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("Select * from estancia where idReservacion="+req.query.idReservacion, function(err, row){
					if(err)
						throw err;
					else
						res.json(row);
					connection.release();	
				});
			});	
		},
		edit:function(req,res){
			var pool=app.get('pool');
			pool.getConnection(function(err,connection){
				if(err){
                    connection.release();
                    res.json({"code" : 100, "status" : "Error al conectar a la base de datos"});
                }
				connection.query("UPDATE reservacion set idUsuario='"+req.body.idUsuario+"',idEstancia="+req.body.idEstancia+"' where reservacion="+req.body.idReservacion, function(err, row){
					if(err)
						throw err;
					else
						res.json({"mensaje":"Reservacion editada"});
					connection.release();	
				});
			});	
		}
	}
}